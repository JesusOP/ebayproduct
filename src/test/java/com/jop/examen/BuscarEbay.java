package com.jop.examen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BuscarEbay extends PageObject {
	ArrayList<Producto> listaProductos = new ArrayList<Producto>();

	public BuscarEbay(WebDriver driver) {
		super(driver);
	}

	@FindBy(id = "gh-ac")
	private WebElement producto;

	@FindBy(id = "gh-btn")
	private WebElement btnBuscar;

	@FindBy(id = "e1-54")
	private WebElement marca;

	@FindBy(id = "e1-29")
	private WebElement talla;

	@FindBy(xpath = "//*[@id=\'cbelm\']/div[3]/h1")
	private WebElement resultadosCantidad;

	@FindBy(id = "DashSortByContainer")
	private WebElement cboascendente;

	@FindBy(xpath = "//*[@id=\'SortMenu\']/li[5]")
	private WebElement ordenPrecio;

	@FindBy(id = "GalleryViewInner")
	private WebElement ordenes;

	public void buscarProducto(String producto) {
		System.out.println("Busqueda de producto :" + producto);
		this.producto.clear();
		this.producto.sendKeys(producto);
		this.producto.submit();
	}

	public void aplicarFiltros() {
		System.out.println("Marca :" + "PUMA");
		System.out.println("Talla :" + "10");
		this.marca.click();
		this.talla.click();
	}

	public void obtenerResultado() {
		System.out.println("Resultado de la busqueda:" + this.resultadosCantidad.getText());
	}

	public void preciosAscendentes() {
		System.out.println("Aplico orden de Precios Ascendentes");
		this.cboascendente.click();
		this.ordenPrecio.click();
	}

	public void takeProducts(int n) {
		System.out.println("Seleccion de " + n + " productos:");
		Producto pro;
		List<WebElement> list = ordenes.findElements(By.tagName("li"));
		for (int i = 0; i < n; i++) {
			pro = new Producto();
			WebElement producto = driver
					.findElement(By.xpath("//*[@id='" + list.get(i).getAttribute("id") + "']/div/div[2]"));
			System.out.println("NOMBRE PRODUCTO " + i + " " + producto.getText());
			pro.setNombre(producto.getText().toString());
			producto = driver
					.findElement(By.xpath("//*[@id='" + list.get(i).getAttribute("id") + "']/div/div[3]/div[2]"));
			pro.setPrecio(producto.getText().substring(0, 12));
			System.out.println("PRECIO PRODUCTO " + i + " " + producto.getText().substring(0, 12));
			listaProductos.add(pro);
		}
	}

	public void listarOrdenes() {
		System.out.println("Realizar Orden de Productos Ascendentes:");
		ArrayList<Producto> lista = listaProductos;
		Collections.sort(lista, new Comparator<Producto>() {
			public int compare(Producto pro1, Producto pro2) {
				return pro1.getNombre().compareTo(pro2.getNombre());
			}
		});
		for (Producto temp : lista) {
			System.out.println(temp.getNombre());
		}
	}

	public Boolean isInitialized() {
		return this.producto.isDisplayed();
	}

}
