package com.jop.examen;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest extends SetUpTest {

	@Test
	public void buscarEbay() {
		driver.get("https://www.ebay.com/");
		
		BuscarEbay buscarEbay = new BuscarEbay(driver);
		assertTrue(buscarEbay.isInitialized());
		
		buscarEbay.buscarProducto("shoes");
		buscarEbay.aplicarFiltros();
		buscarEbay.obtenerResultado();
		
		buscarEbay.preciosAscendentes();
		
		buscarEbay.takeProducts(5);
		buscarEbay.listarOrdenes();
		
		
	}

}
